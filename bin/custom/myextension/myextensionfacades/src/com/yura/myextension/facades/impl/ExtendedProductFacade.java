package com.yura.myextension.facades.impl;

import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductVariantFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;

public class ExtendedProductFacade extends DefaultProductVariantFacade {

    private static final String MIN_RATING_KEY = "myextensionfacades.min.rating";

    private ConfigurationService configService;

    public ExtendedProductFacade(ConfigurationService configService) {
        this.configService = configService;
    }

    @Override
    public ReviewData postReview(String productCode, ReviewData reviewData) {
        Double minRating = getMinRating();

        if (reviewData == null || reviewData.getRating().compareTo(minRating) < 0) {
            return reviewData;
        }

        return super.postReview(productCode, reviewData);
    }

    private double getMinRating() {
        return Double.parseDouble(configService.getConfiguration().getString(MIN_RATING_KEY));
    }
}
