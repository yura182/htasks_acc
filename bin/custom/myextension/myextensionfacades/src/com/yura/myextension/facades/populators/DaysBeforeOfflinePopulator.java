package com.yura.myextension.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class DaysBeforeOfflinePopulator implements Populator<ProductModel, ProductData> {

    @Override
    public void populate(ProductModel productModel, ProductData productData) throws ConversionException {
        if (productModel.getOfflineDate() == null) {
            return;
        }

        int daysBeforeOffline = getDaysBeforeOffline(productModel);

        if (daysBeforeOffline <= 0) {
            return;
        }

        productData.setDaysBeforeOffline(daysBeforeOffline);
    }

    private int getDaysBeforeOffline(ProductModel productModel) {
        return Period.between(LocalDate.now(), getOfflineDate(productModel)).getDays();
    }

    private LocalDate getOfflineDate(ProductModel productModel) {
        return productModel.getOfflineDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
