package com.yura.myextension.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DaysBeforeOfflinePopulatorTest {

    private static final Integer DAYS_BEFORE_OFFLINE = 10;
    private static final int NEGATIVE_DAYS_BEFORE_OFFLINE = -10;

    @Mock
    private ProductModel productModel;

    private DaysBeforeOfflinePopulator populator;

    private ProductData productData;

    @Before
    public void setup() {
        populator = new DaysBeforeOfflinePopulator();
        productData = new ProductData();
    }

    @Test
    public void shouldPopulateDays() {
        when(productModel.getOfflineDate()).thenReturn(getMockOfflineDate(DAYS_BEFORE_OFFLINE));

        populator.populate(productModel, productData);

        assertEquals(DAYS_BEFORE_OFFLINE, productData.getDaysBeforeOffline());
    }

    @Test
    public void shouldNotPopulateForNullOfflineDate() {
        when(productModel.getOfflineDate()).thenReturn(null);

        populator.populate(productModel, productData);

        assertNull(productData.getDaysBeforeOffline());
    }

    @Test
    public void shouldNotPopulateForZeroOfflineDays() {
        when(productModel.getOfflineDate()).thenReturn(new Date());

        populator.populate(productModel, productData);

        assertNull(productData.getDaysBeforeOffline());
    }

    @Test
    public void shouldNotPopulateForNegativeOfflineDays() {
        when(productModel.getOfflineDate()).thenReturn(getMockOfflineDate(NEGATIVE_DAYS_BEFORE_OFFLINE));

        populator.populate(productModel, productData);

        assertNull(productData.getDaysBeforeOffline());
    }

    private Date getMockOfflineDate(Integer days) {
        return Date.from(LocalDate.now()
                .plusDays(days)
                .atStartOfDay(ZoneId.systemDefault())
                .toInstant());
    }
}
