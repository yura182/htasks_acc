package com.yura.myextension.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdditionalInformationPopulatorTest {

    private static final String ADDITIONAL_INFORMATION = "This is additional information";

    @Mock
    private ProductModel productModel;

    private ProductData productData;

    private AdditionalInformationPopulator populator;

    @Before
    public void setup() {
        productData = new ProductData();
        populator = new AdditionalInformationPopulator();
    }

    @Test
    public void shouldPopulateAdditionalInformation() {
        when(productModel.getAdditionalInformation()).thenReturn(ADDITIONAL_INFORMATION);
        populator.populate(productModel, productData);

        assertEquals(ADDITIONAL_INFORMATION, productData.getAdditionalInformation());
    }

    @Test
    public void shouldPopulateNullAsAdditionalInformation() {
        when(productModel.getAdditionalInformation()).thenReturn(null);
        populator.populate(productModel, productData);

        assertNull(productData.getAdditionalInformation());
    }
}
