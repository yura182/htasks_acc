<div class="myextension-content">
    <img src="${media.url}" alt="${media.altText}"/>
    <div class="myextension-text">
    <p class="title">${title}</p>
    ${content}
    </div>

</div>
