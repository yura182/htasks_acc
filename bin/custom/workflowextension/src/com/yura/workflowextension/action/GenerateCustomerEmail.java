package com.yura.workflowextension.action;

import com.yura.workflowextension.model.EmailNotificationProcessModel;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateCustomerEmail extends AbstractProceduralAction<EmailNotificationProcessModel> {

    private static final String FROM_EMAIL = Config.getString("mail.from", "default@gmail.com");
    private static final String DISPLAY_NAME = Config.getString("mail.display.name", "Manager");
    private static final String REPLY_TO_EMAIL = Config.getString("mail.replyto", "default@gmail.com");
    private static final String MAIL_SUBJECT = Config.getString("mail.update.profile.subject", "Updated");
    private static final String MAIL_BODY = Config.getString("mail.update.profile.body", "Was updated");

    private ModelService modelService;
    private EmailService emailService;

    public GenerateCustomerEmail(ModelService modelService, EmailService emailService) {
        this.modelService = modelService;
        this.emailService = emailService;
    }

    @Override
    public void executeAction(EmailNotificationProcessModel emailNotificationProcessModel) throws RetryLaterException, Exception {
        EmailMessageModel emailMessageModel = getEmailMessageModel(emailNotificationProcessModel);

        updateEmailList(emailNotificationProcessModel, emailMessageModel);

        modelService.save(emailNotificationProcessModel);
    }

    private void updateEmailList(EmailNotificationProcessModel emailNotificationProcessModel, EmailMessageModel emailMessageModel) {
        List<EmailMessageModel> emails = new ArrayList<>(emailNotificationProcessModel.getEmails());
        emails.add(emailMessageModel);
        emailNotificationProcessModel.setEmails(emails);
    }

    private EmailMessageModel getEmailMessageModel(EmailNotificationProcessModel emailNotificationProcessModel) {
        EmailMessageModel emailMessageModel = modelService.create(EmailMessageModel.class);
        emailMessageModel.setFromAddress(emailService.getOrCreateEmailAddressForEmail(FROM_EMAIL, DISPLAY_NAME));
        emailMessageModel.setReplyToAddress(REPLY_TO_EMAIL);
        emailMessageModel.setToAddresses(getAddresses(emailNotificationProcessModel));
        emailMessageModel.setSubject(MAIL_SUBJECT);
        emailMessageModel.setBody(MAIL_BODY);
        return emailMessageModel;
    }

    private List<EmailAddressModel> getAddresses(EmailNotificationProcessModel emailNotificationProcessModel) {
        return Collections.singletonList(emailService.getOrCreateEmailAddressForEmail(emailNotificationProcessModel.getEmail(), ""));
    }
}
