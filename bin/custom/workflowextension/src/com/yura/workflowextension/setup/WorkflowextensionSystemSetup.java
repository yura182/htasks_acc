/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.yura.workflowextension.setup;

import static com.yura.workflowextension.constants.WorkflowextensionConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.yura.workflowextension.constants.WorkflowextensionConstants;
import com.yura.workflowextension.service.WorkflowextensionService;


@SystemSetup(extension = WorkflowextensionConstants.EXTENSIONNAME)
public class WorkflowextensionSystemSetup
{
	private final WorkflowextensionService workflowextensionService;

	public WorkflowextensionSystemSetup(final WorkflowextensionService workflowextensionService)
	{
		this.workflowextensionService = workflowextensionService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		workflowextensionService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return WorkflowextensionSystemSetup.class.getResourceAsStream("/workflowextension/sap-hybris-platform.png");
	}
}
