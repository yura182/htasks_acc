package com.yura.workflowextension.cache.impl;

import com.yura.workflowextension.cache.CacheUtility;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.region.CacheRegion;
import de.hybris.platform.util.Utilities;

public class DefaultCacheUtilityImpl implements CacheUtility {

    private CacheController cacheController;

    public DefaultCacheUtilityImpl(CacheController cacheController) {
        this.cacheController = cacheController;
    }

    @Override
    public void invalidateCachedItem(ItemModel model) {
        Utilities.invalidateCache(model.getPk());
    }

    @Override
    public void clearCacheRegion(CacheRegion cacheRegion) {
        cacheController.clearCache(cacheRegion);
    }

    @Override
    public void clearAllCacheRegions() {
        cacheController.getRegions().forEach(CacheRegion::clearCache);
    }
}
