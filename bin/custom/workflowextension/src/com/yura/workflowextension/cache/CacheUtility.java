package com.yura.workflowextension.cache;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.regioncache.region.CacheRegion;

public interface CacheUtility {

    void invalidateCachedItem(ItemModel model);

    void clearCacheRegion(CacheRegion cacheRegion);

    void clearAllCacheRegions();
}
