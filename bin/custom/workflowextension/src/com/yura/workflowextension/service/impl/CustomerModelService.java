package com.yura.workflowextension.service.impl;

import com.yura.workflowextension.model.EmailNotificationProcessModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import org.springframework.beans.factory.annotation.Required;

public class CustomerModelService extends DefaultCustomerAccountService {
    private static final String WORKFLOW_NAME_KEY = "workflow.template.name";

    private WorkflowService workflowService;
    private WorkflowTemplateService workflowTemplateService;
    private WorkflowProcessingService workflowProcessingService;
    private BusinessProcessService businessProcessService;

    @Override
    public void register(CustomerModel customerModel, String password) throws DuplicateUidException {
        super.register(customerModel, password);

        startWorkflow(customerModel);
    }

    @Override
    public void updateProfile(CustomerModel customerModel, String titleCode, String name, String login) throws DuplicateUidException {
        super.updateProfile(customerModel, titleCode, name, login);
        EmailNotificationProcessModel process = businessProcessService.createProcess("EmailNotificationProcess-" + customerModel.getCustomerID() + "-" + System.currentTimeMillis(),
                "EmailNotificationProcess");
        process.setEmail(customerModel.getContactEmail());
        getModelService().save(process);
        businessProcessService.startProcess(process);
    }

    private void startWorkflow(CustomerModel customerModel) {
        WorkflowTemplateModel workflowTemplateModel = workflowTemplateService
                .getWorkflowTemplateForCode(getWorkflowName());
        WorkflowModel workflowModel = workflowService
                .createWorkflow(workflowTemplateModel, customerModel, getUserService().getAdminUser());

        getModelService().save(workflowModel);

        workflowModel.getActions().forEach(a -> getModelService().save(a));

        workflowProcessingService.startWorkflow(workflowModel);
    }

    private String getWorkflowName() {
        return getConfigurationService().getConfiguration().getString(WORKFLOW_NAME_KEY);
    }

    @Required
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Required
    public void setWorkflowTemplateService(WorkflowTemplateService workflowTemplateService) {
        this.workflowTemplateService = workflowTemplateService;
    }

    @Required
    public void setWorkflowProcessingService(WorkflowProcessingService workflowProcessingService) {
        this.workflowProcessingService = workflowProcessingService;
    }

    @Required
    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }
}
