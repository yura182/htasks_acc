package com.yura.workflowextension.service.impl;

import com.yura.workflowextension.service.ProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class CustomProductServiceImpl extends DefaultProductService implements ProductService {

    @Override
    @Transactional
    public void updateManufacturerNameForProducts(String manufacturerName, List<ProductModel> products) {
        products.forEach(productModel -> {
            productModel.setManufacturerName(manufacturerName);
            getModelService().save(productModel);
        });
    }
}
