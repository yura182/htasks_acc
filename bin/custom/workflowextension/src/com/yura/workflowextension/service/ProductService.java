package com.yura.workflowextension.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductService {

    void updateManufacturerNameForProducts(String manufacturerName, List<ProductModel> products);
}
