package com.yura.workflowextension.workflow;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.Collections;
import java.util.Optional;

public abstract class AbstractCustomerRegistrationActionJob implements AutomatedWorkflowTemplateJob {

    private ModelService modelService;

    public AbstractCustomerRegistrationActionJob(ModelService modelService) {
        this.modelService = modelService;
    }

    protected CustomerModel getAttachedCustomer(WorkflowActionModel actionModel) {

        return Optional.ofNullable(actionModel.getAttachmentItems())
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(i -> i instanceof CustomerModel)
                .map(i -> (CustomerModel) i)
                .findAny()
                .orElse(null);
    }

    public ModelService getModelService() {
        return modelService;
    }
}
