package com.yura.workflowextension.workflow;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegistrationConfirmationActionJob extends AbstractCustomerRegistrationActionJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationConfirmationActionJob.class);

    public RegistrationConfirmationActionJob(ModelService modelService) {
        super(modelService);
    }

    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        CustomerModel customerModel = getAttachedCustomer(workflowActionModel);

        if (!customerModel.isConfirmed()) {
            customerModel.setConfirmed(true);
            getModelService().save(customerModel);
        }

        LOGGER.info("Customer {} confirmed.", customerModel.getUid());

        return workflowActionModel.getDecisions()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
