package com.yura.workflowextension.workflow;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegistrationDeclineActionJob extends AbstractCustomerRegistrationActionJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationDeclineActionJob.class);

    public RegistrationDeclineActionJob(ModelService modelService) {
        super(modelService);
    }

    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        CustomerModel customerModel = getAttachedCustomer(workflowActionModel);

        LOGGER.info("Customer {} was declined and will be removed", customerModel.getUid());

        getModelService().remove(customerModel);

        return workflowActionModel.getDecisions()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
