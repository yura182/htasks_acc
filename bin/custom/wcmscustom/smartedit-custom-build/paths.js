/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
module.exports = function() {

    /***
     *  Naming:
     *  File or Files masks should end in File or Files,
     *  ex: someRoot.path.myBlaFiles = /root/../*.*
     *
     *  General rules:
     *  No copy paste
     *  No duplicates
     *  Avoid specific files when possible, try to specify folders
     *  What happens to wcmscustom, happens to wcmscustomContainer
     *  Try to avoid special cases and exceptions
     */
    var lodash = require('lodash');

    var paths = {};

    paths.tests = {};

    paths.tests.root = 'jsTests';
    paths.tests.testsRoot = paths.tests.root + '/tests';
    paths.tests.wcmscustomContainerTestsRoot = paths.tests.testsRoot + '/wcmscustomContainer';
    paths.tests.wcmscustomContainere2eTestsRoot = paths.tests.wcmscustomContainerTestsRoot + '/e2e';

    paths.tests.wcmscustome2eTestFiles = paths.tests.root + '/e2e/**/*Test.js';
    paths.tests.wcmscustomContainere2eTestFiles = paths.tests.wcmscustomContainere2eTestsRoot + '/**/*Test.js';

    paths.e2eFiles = [
        paths.tests.wcmscustome2eTestFiles,
        paths.tests.wcmscustomContainere2eTestFiles
    ];

    return paths;

}();
