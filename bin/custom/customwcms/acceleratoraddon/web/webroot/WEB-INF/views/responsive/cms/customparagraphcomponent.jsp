<div class="content">
    <img src="${media.url}" alt="${media.altText}"/>
    ${title}
    ${content}
</div>
